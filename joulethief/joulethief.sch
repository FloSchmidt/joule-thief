EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Battery_Cell BT1
U 1 1 61225407
P 5050 2800
F 0 "BT1" H 5168 2896 50  0000 L CNN
F 1 "Battery_Cell" H 5168 2805 50  0000 L CNN
F 2 "" V 5050 2860 50  0001 C CNN
F 3 "~" V 5050 2860 50  0001 C CNN
	1    5050 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 612254FC
P 6700 2500
F 0 "R1" H 6770 2546 50  0000 L CNN
F 1 "R" H 6770 2455 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 6630 2500 50  0001 C CNN
F 3 "~" H 6700 2500 50  0001 C CNN
	1    6700 2500
	1    0    0    -1  
$EndComp
$Comp
L Device:L_Core_Ferrite_Coupled L1
U 1 1 612257E2
P 6900 1850
F 0 "L1" V 6946 1662 50  0000 R CNN
F 1 "L_Core_Ferrite_Coupled" V 6855 1662 50  0000 R CNN
F 2 "Transformer_THT:Transformer_Toroid_Horizontal_D14.0mm_Amidon-T50" H 6900 1850 50  0001 C CNN
F 3 "~" H 6900 1850 50  0001 C CNN
	1    6900 1850
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D1
U 1 1 61225881
P 7750 3100
F 0 "D1" V 7788 2983 50  0000 R CNN
F 1 "LED" V 7697 2983 50  0000 R CNN
F 2 "LED_THT:LED_D5.0mm_Clear" H 7750 3100 50  0001 C CNN
F 3 "~" H 7750 3100 50  0001 C CNN
	1    7750 3100
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 61225F6B
P -743800 -415450
F 0 "SW1" H -743800 -415165 50  0000 C CNN
F 1 "SW_Push" H -743800 -415256 50  0000 C CNN
F 2 "" H -743800 -415250 50  0001 C CNN
F 3 "" H -743800 -415250 50  0001 C CNN
	1    -743800 -415450
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 61226769
P 5600 1650
F 0 "SW2" H 5600 1935 50  0000 C CNN
F 1 "SW_Push" H 5600 1844 50  0000 C CNN
F 2 "" H 5600 1850 50  0001 C CNN
F 3 "" H 5600 1850 50  0001 C CNN
	1    5600 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 61226851
P 6100 2750
F 0 "C1" H 6218 2796 50  0000 L CNN
F 1 "CP" H 6218 2705 50  0000 L CNN
F 2 "joulethief:C_Axial_L5mm_D19mm_P5.08mm_Horizontal" H 6138 2600 50  0001 C CNN
F 3 "~" H 6100 2750 50  0001 C CNN
	1    6100 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 3250 7000 3550
Wire Wire Line
	7750 2850 7750 2950
Wire Wire Line
	7750 3250 7750 3550
Wire Wire Line
	7750 3550 7000 3550
$Comp
L Transistor_BJT:BC549 Q1
U 1 1 6122566E
P 6900 3050
F 0 "Q1" H 7091 3096 50  0000 L CNN
F 1 "BC549" H 7091 3005 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_HandSolder" H 7100 2975 50  0001 L CIN
F 3 "http://www.fairchildsemi.com/ds/BC/BC547.pdf" H 6900 3050 50  0001 L CNN
	1    6900 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	7000 2050 7000 2850
Wire Wire Line
	7000 2850 7750 2850
Connection ~ 7000 2850
Wire Wire Line
	6700 3050 6700 2650
Wire Wire Line
	6700 2350 6700 2050
Wire Wire Line
	6700 2050 6800 2050
Wire Wire Line
	5800 1650 6100 1650
Wire Wire Line
	6800 1650 7000 1650
Connection ~ 6800 1650
Wire Wire Line
	6100 1650 6100 2600
Connection ~ 6100 1650
Wire Wire Line
	6100 1650 6800 1650
Wire Wire Line
	6100 2900 6100 3550
Wire Wire Line
	6100 3550 7000 3550
Connection ~ 7000 3550
Wire Wire Line
	6100 3550 5050 3550
Wire Wire Line
	5050 3550 5050 2900
Connection ~ 6100 3550
Wire Wire Line
	5050 2600 5050 1650
Wire Wire Line
	5050 1650 5200 1650
$Comp
L Device:Battery_Cell BT2
U 1 1 615ECC20
P 4650 2800
F 0 "BT2" H 4768 2896 50  0000 L CNN
F 1 "Battery_Cell" H 4768 2805 50  0000 L CNN
F 2 "Battery:BatteryHolder_Keystone_2466_1xAAA" V 4650 2860 50  0001 C CNN
F 3 "~" V 4650 2860 50  0001 C CNN
	1    4650 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 2600 4650 1650
Wire Wire Line
	4650 1650 5050 1650
Connection ~ 5050 1650
Wire Wire Line
	4650 2900 4650 3550
Wire Wire Line
	4650 3550 5050 3550
Connection ~ 5050 3550
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 615F56B4
P 5000 1000
F 0 "J1" H 5108 1181 50  0000 C CNN
F 1 "Conn_01x02_Male" H 5108 1090 50  0000 C CNN
F 2 "" H 5000 1000 50  0001 C CNN
F 3 "~" H 5000 1000 50  0001 C CNN
	1    5000 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 1000 6100 1000
Wire Wire Line
	6100 1000 6100 1650
Wire Wire Line
	5200 1100 5200 1650
Connection ~ 5200 1650
Wire Wire Line
	5200 1650 5400 1650
$EndSCHEMATC
